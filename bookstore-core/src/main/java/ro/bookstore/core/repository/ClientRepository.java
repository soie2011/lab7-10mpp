package ro.bookstore.core.repository;

import ro.bookstore.core.model.Client;

/**
 * Created by Oana on 4/24/2017.
 */
public interface ClientRepository extends BookstoreRepository<Client, Long> {
}
