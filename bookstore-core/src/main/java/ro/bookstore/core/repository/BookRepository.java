package ro.bookstore.core.repository;

import ro.bookstore.core.model.Book;

/**
 * Created by Oana on 4/10/2017.
 */
public interface BookRepository extends BookstoreRepository<Book, Long>{
}
