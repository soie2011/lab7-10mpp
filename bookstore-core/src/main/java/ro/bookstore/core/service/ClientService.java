package ro.bookstore.core.service;

import ro.bookstore.core.model.Client;

import java.util.List;

public interface ClientService {

    List<Client> findAll();

    Client updateClient(Long clientId, String name, String phoneNb, String gender, Integer age);
    static void deleteClient(Long clientId);
    Client createClient(String name,String phoneNb,String gender,Integer age);

}
