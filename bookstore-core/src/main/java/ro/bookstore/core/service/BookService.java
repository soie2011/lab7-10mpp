package ro.bookstore.core.service;

import ro.bookstore.core.model.Book;

import java.util.List;

/**
 * Created by Oana on 4/10/2017.
 */
public interface BookService {

    List<Book> findAll();
}
