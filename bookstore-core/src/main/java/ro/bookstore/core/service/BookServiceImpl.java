package ro.bookstore.core.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.bookstore.core.model.Book;
import ro.bookstore.core.repository.BookRepository;

import java.util.List;

/**
 * Created by Oana on 4/10/2017.
 */

@Service
public class BookServiceImpl implements BookService {

    private static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    @Transactional
    public List<Book> findAll() {
        log.trace("findAll");

        List<Book> books = bookRepository.findAll();

        log.trace("findAll: students={}", books);

        return books;
    }
}
