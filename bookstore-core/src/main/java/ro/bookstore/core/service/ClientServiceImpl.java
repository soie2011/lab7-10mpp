package ro.bookstore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.bookstore.core.model.Client;
import ro.bookstore.core.repository.ClientRepository;

import java.util.List;

/**
 * Created by Oana on 4/24/2017.
 */
@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    @Transactional
    public List<Client> findAll() {
        log.trace("findAll");

        List<Client> clients = clientRepository.findAll();

        log.trace("findAll: clients={}", clients);

        return clients;
    }

    @Transactional
    public Client updateClient(Long clientId, String name, String phoneNb, String gender, Integer age) {
        log.trace("updateClient: ClientId={}, serialName={}, phoneNb={}, gender={},age={}",
                clientId,name,phoneNb,gender,age);

        Client client = clientRepository.findOne(clientId);
        client.setName(name);
        client.setPhoneNb(phoneNb);
        client.setAge(age);
        client.setGender(gender);

        log.trace("updateClient: Client={}", client);

        return client;
    }


    @Override
    @Transactional
    public void deleteClient(Long clientId) {
        log.trace("deleteClient: ClientId={}", clientId);

        clientRepository.delete(clientId);

        log.trace("deleteClient - method end");

    }

    @Override
    public Client createClient(String name, String phoneNb, String gender, Integer age) {
        log.trace("updateClient: ClientId={}, serialName={}, phoneNb={}, gender={},age={}",
                name,phoneNb,gender,age);

        Client client = new Client(name,phoneNb,gender,age);
        client = clientRepository.save(client);

        log.trace("createClient: Client={}", client);

        return client;
    }
}
