package ro.bookstore.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Oana on 4/10/2017.
 */
@Entity
@Table(name = "books")
public class Book extends BaseEntity<Long> {

    @Column(name = "category", nullable = false)
    private String category;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "year", nullable = false)
    private Integer year;

    @Column(name = "price", nullable = false)
    private float price;


    public Book(){}

    public Book(String category, String title, String author, Integer year, float price){
        this.category = category;
        this.title = title;
        this.author = author;
        this.year = year;
        this.price = price;
    }

    public String getCategory() { return category; }

    public void setCategory(String category){ this.category = category; }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getYear() { return year; }

    public void setYear(Integer year) {
        this.year = year;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString(){
        return "Book{category: "
                + this.getCategory()
                + ", title: " + this.getTitle()
                + ", author: " + this.getAuthor()
                + ", year: " + this.getYear()
                + ", price: " + this.getPrice()+ " $ } "
                + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (!title.equals(book.title)) return false;
        if (!author.equals(book.author)) return false;
        if (year != book.year) return false;
        if (!category.equals(book.category)) return false;

        return (price == book.price);
    }

}
