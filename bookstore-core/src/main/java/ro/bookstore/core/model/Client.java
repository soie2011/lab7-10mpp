package ro.bookstore.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Oana on 4/24/2017.
 */
@Entity
@Table(name = "clients")
public class Client extends BaseEntity<Long> {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "phoneNb", nullable = false)
    private String phoneNb;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "age", nullable = false)
    private Integer age;

    public Client(){}

    public Client(String name, String phoneNb, String gender, Integer age){
        this.name = name;
        this.phoneNb = phoneNb;
        this.gender = gender;
        this.age = age;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setPhoneNb(String phoneNb){
        this.phoneNb = phoneNb;
    }

    public String getPhoneNb(){
        return this.phoneNb;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

    public String getGender(){
        return this.gender;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    /**
     * Provides a string representation of the Client object.
     * @return a string with all the values of the fields for that object.
     */
    @Override
    public String toString(){
        return "Client{name: "
                + this.getName()
                + ", phoneNb: " + this.phoneNb
                + ", gender: " + this.getGender()
                + ", age: " + this.getAge() + "} "
                + super.toString();
    }

    /**
     * Determines whether the current object is equal to the given object.
     * @param o
     *  is the object to be compared with the current one.
     * @return
     *  true if the given object has the same value for each field as the current object, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (gender != client.gender) return false;
        if (!phoneNb.equals(client.phoneNb)) return false;
        return name.equals(client.name);

    }
}
