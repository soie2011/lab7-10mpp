import {Component, OnInit, Input} from '@angular/core';
import { BookService } from '../shared/book.service'
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from '@angular/common';
import { Book } from '../shared/book.model'

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  @Input()
  book: Book;

  constructor(private bookService: BookService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.bookService.getBook(+params['id']))
      .subscribe(book => this.book = book);
  }

  goBack(): void {
    this.location.back();
  }

}
