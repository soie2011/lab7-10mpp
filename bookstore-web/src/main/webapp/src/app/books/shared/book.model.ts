export class Book {
  id: number;
  category: string;
  title: string;
  author: string;
  year: number;
  price: number;
}
