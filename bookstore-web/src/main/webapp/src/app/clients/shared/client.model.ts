export class Client {
  id: number;
  name: string;
  phoneNb: string;
  gender: string;
  age: number;
}
