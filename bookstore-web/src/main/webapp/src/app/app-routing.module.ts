import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {BooksComponent} from "./books/books.component";
import {BookDetailsComponent} from "./books/book-details/book-details.component";
import {ClientsComponent} from "./clients/clients.component";
import {ClientDetailsComponent} from "./clients/client-details/client-details.component";
const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'books',     component: BooksComponent },
  { path: 'book/detail/:id', component: BookDetailsComponent},
  { path: 'clients', component: ClientsComponent },
  { path: 'client/detail/:id', component : ClientDetailsComponent}

];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
