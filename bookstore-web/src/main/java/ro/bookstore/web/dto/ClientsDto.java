package ro.bookstore.web.dto;

import ro.bookstore.core.model.Client;

import java.util.List;

/**
 * Created by Oana on 4/24/2017.
 */
public class ClientsDto {

    private List<Client> clients;

    public ClientsDto(){}

    public ClientsDto(List<Client> clients){
        this.clients = clients;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
}
