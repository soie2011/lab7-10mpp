package ro.bookstore.web.dto;

/**
 * Created by Toma Oana on 5/2/2017.
 */
public class ClientDto extends BaseDto {
    private String name;
    private String phoneNb;
    private String gender;
    private Integer age;

    public ClientDto(String name, String phoneNb, String gender, Integer age) {
        super();
        this.name=name;
        this.phoneNb = phoneNb;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString(){
        return "Client{name: "
                + this.name
                + ", phoneNb: " + this.phoneNb
                + ", gender: " + this.gender
                + ", age: " + this.age + "} "
                + super.toString();
    }


    public String getName() {
        return name;
    }

    public String getPhoneNb() {
        return phoneNb;
    }

    public String getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }
}
