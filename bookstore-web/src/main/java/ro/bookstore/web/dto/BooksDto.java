package ro.bookstore.web.dto;

import ro.bookstore.core.model.Book;

import java.util.List;

/**
 * Created by Oana on 4/10/2017.
 */
public class BooksDto {

    private List<Book> books;

    public BooksDto() {
    }

    public BooksDto(List<Book> books) {
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
