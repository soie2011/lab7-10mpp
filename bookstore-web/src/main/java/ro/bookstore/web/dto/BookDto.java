package ro.bookstore.web.dto;

/**
 * Created by Toma Oana on 5/2/2017.
 */
public class BookDto {
    private String category;
    private String title;
    private String author;
    private Integer year;
    private float price;

    @Override
    public String toString(){
        return "Book{category: "
                + this.category
                + ", title: " + this.title
                + ", author: " + this.author
                + ", year: " + this.year
                + ", price: " + this.price+ " $ } "
                + super.toString();
    }

}
