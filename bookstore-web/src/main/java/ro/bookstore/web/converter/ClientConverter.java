package ro.bookstore.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.bookstore.core.model.Client;
import ro.bookstore.web.dto.ClientDto;

/**
 * Created by Toma Oana on 5/2/2017.
 */
public class ClientConverter extends BaseConverter<Client,ClientDto>{
    private static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    @Override
    public ClientDto convertModelToDto(Client  client) {
        ClientDto clientDto = new ClientDto(client.getName(),client.getPhoneNb(), client.getGender(), client.getAge());
        clientDto.setId(client.getId());
        return clientDto;
    }
}
