package ro.bookstore.web.converter;
import ro.bookstore.core.model.BaseEntity;
import ro.bookstore.web.dto.BaseDto;


public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto> {

    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);

}

