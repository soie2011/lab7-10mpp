package ro.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.bookstore.core.model.Book;
import ro.bookstore.core.service.BookService;
import ro.bookstore.web.dto.BooksDto;

import java.util.List;

/**
 * Created by Oana on 4/10/2017.
 */

@RestController
public class BookController {

    private static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/books", produces = MediaType.APPLICATION_JSON_VALUE)
    public BooksDto getBooks() {
        log.trace("getBooks");

        List<Book> books = bookService.findAll();

        log.trace("getBooks: books={}", books);

        return new BooksDto(books);
    }
}
