package ro.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.bookstore.core.model.Client;
import ro.bookstore.core.service.ClientService;
import ro.bookstore.web.converter.ClientConverter;
import ro.bookstore.web.dto.ClientDto;
import ro.bookstore.web.dto.ClientsDto;
import ro.bookstore.web.dto.EmptyJsonResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Oana on 4/24/2017.
 */
@RestController
public class ClientController {

    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
    public ClientsDto getClients(){
        log.trace("getClients");

        List<Client> clients = clientService.findAll();

        log.trace("getClients: clients={}", clients);

        return new ClientsDto(clients);
    }

    @RequestMapping(value = "/Clients/{ClientId}", method = RequestMethod.PUT)
    public Map<String, ClientDto> updateClient(
            @PathVariable final Long ClientId,
            @RequestBody final Map<String, ClientDto> clientDtoMap) {
        log.trace("updateClient: ClientId={}, ClientDtoMap={}", ClientId, clientDtoMap);

        ClientDto clientDto = clientDtoMap.get("Client");
        Client client = clientService.updateClient(ClientId,clientDto.getName(),clientDto.getPhoneNb(), clientDto.getGender(), clientDto.getAge());


        Map<String, ClientDto> result = new HashMap<>();
        result.put("Client", clientConverter.convertModelToDto(client));

        log.trace("updateClient: result={}", result);

        return result;
    }

    @RequestMapping(value = "/Clients", method = RequestMethod.POST)
    public Map<String, ClientDto> createClient(
            @RequestBody final Map<String, ClientDto> clientDtoMap) {
        log.trace("createClient: ClientDtoMap={}", clientDtoMap);

        ClientDto clientDto = clientDtoMap.get("Client");
        Client client = clientService.createClient(clientDto.getName(),clientDto.getPhoneNb(), clientDto.getGender(), clientDto.getAge());


        Map<String, ClientDto> result = new HashMap<>();
        result.put("Client",  clientConverter.convertModelToDto(client));

        log.trace("updateClient: result={}", result);

        return result;
    }

    @RequestMapping(value = "Clients/{ClientId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteClient(@PathVariable final Long ClientId) {
        log.trace("deleteClient: ClientId={}", ClientId);

        ClientService.deleteClient(ClientId);

        log.trace("deleteClient - method end");

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }
}
